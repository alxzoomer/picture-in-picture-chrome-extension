# Picture-in-Picture Chrome Extension

This extension is modification of original code [https://github.com/GoogleChromeLabs/picture-in-picture-chrome-extension](https://github.com/GoogleChromeLabs/picture-in-picture-chrome-extension).

In this version has the following changes:

- removed Google Analytics tracking and configuration
- added ability to open picture-in-picture for video with attribute `disablepictureinpicture` so it works well on ivi.ru.

## Installation

This extension is not uploaded to Chrome Web Store and can be installed in Chrome Developer mode:

- `git clone https://gitlab.com/alxzoomer/zoomer-pw.git`
- open Chrome settings
- click **Extensions** menu
- click **Developer mode**
- click **Load unpacked** and select **src** directory
- click **Extensions** button in Chrome toolbar
- enjoy!

## Configuration

The keyboard shortcut (defaults to `Alt-P`) can be changed on the
Chrome Extension Shortcuts settings page:
chrome://extensions/shortcuts
